package src;

import java.util.Scanner;

public class Meter {

	public static void main(String[] args) {
		double meter; // 1m = 3.2808399 feet
		double feet; // 1feet = 0.3048 meters

		Scanner input = new Scanner(System.in);
		System.out.println("Enter a number in meter");
		meter = input.nextDouble();
		feet = meter * 3.2808399;
		System.out.println(meter + " meter is " + feet + " feet");

		System.out.println("Enter a number in feet");
		feet = input.nextDouble();
		meter = feet * 0.3048;
		System.out.println(feet + " feet is " + meter + " meter");
		input.close();
	}

}
