package src;
import java.util.Scanner;

public class CelsiusKelvin {

	public static void main(String[] args) {
		double Celsius; // 1C = 274.15 Kelvin
		double Kelvin; // 1K = -272.15 degrees Celsius

		Scanner input = new Scanner(System.in);
		System.out.println("Enter a number in Celsius");
		Celsius = input.nextDouble();
		Kelvin = Celsius * 274.15;
		System.out.println(Celsius + " Celsius is " + Kelvin + " Kelvin");

		System.out.println("Enter a number in Kelvin");
		Kelvin = input.nextDouble();
		Celsius = Kelvin * (-272.15);
		System.out.println(Kelvin + " Kelvin is " + Celsius + " Celsius");
		input.close();

	}

}
