package src;
import java.util.Scanner;

public class SquareMeter {

	public static void main(String[] args) {
		double squaremeter; // 1sm = 1.0 � 10--6 square kilometers
		double squarekilometer; // 1skm = 1 000 000 square meters

		Scanner input = new Scanner(System.in);
		System.out.println("Enter a number in squaremeter");
		squaremeter = input.nextDouble();
		squarekilometer = squaremeter * 1_000_000;
		System.out.println(squaremeter + " squaremeter is " + squarekilometer + " squarekilometer");

		System.out.println("Enter a number in squarekilometer");
		squarekilometer = input.nextDouble();
		squaremeter = squarekilometer / 1_000_000; 
		System.out.println(squarekilometer + " squarekilometer is " + squaremeter + " squaremeter");
		input.close();
	}

}
