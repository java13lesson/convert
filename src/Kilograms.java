package src;
import java.util.Scanner;

public class Kilograms {

	
	public static void main(String[] args) {
	
		double pounds; //kilograms * 2.205
		double kilograms; //pounds * 0.454
		
		Scanner input = new Scanner (System.in);
		System.out.println("Enter a number in pounds");
		pounds = input.nextDouble();
		kilograms = pounds * 0.454;
		System.out.println(pounds +  " pounds is " + kilograms + " kilograms" );
		

		
		System.out.println("Enter a number in kilograms");
		kilograms = input.nextDouble();
		pounds = kilograms * 2.205;
		System.out.println(kilograms +  " kilograms is " + pounds + " pounds" );
		input.close();
	}

}
