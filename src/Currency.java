package src;

import java.util.Scanner;

public class Currency {

	public static void main(String[] args) {
		double dollars; // 1 dollar = 0.82 eur
		double euro; // 1 euro = 1.21 usd
		
		Scanner input = new Scanner (System.in);
		System.out.println("Enter a number in dollars");
		dollars = input.nextDouble();
		euro = dollars * 0.82;
		System.out.println(dollars +  " dollars is " + euro + " euro" );
		
		System.out.println("Enter a number in euro");
		euro = input.nextDouble();
		dollars = euro * 1.21;
		System.out.println(euro +  " euro is " + dollars + " dollars" );
		input.close();
}
}