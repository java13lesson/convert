package src;

import java.util.Scanner;

public class CelsiusFarengheit {

	public static void main(String[] args) {
		
		double Celsius; //1C = 33.8 degrees Fahrenheit
		double Farengheit; //1F = -17.2222222 degrees Celsius
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a number in Celsius");
		Celsius = input.nextDouble();
		Farengheit = Celsius * 33.8;
		System.out.println(Celsius + " Celsius is " + Farengheit + " Farengheit ");
		
		System.out.println("Enter a number in Farengheit");
		Farengheit = input.nextDouble();
		Celsius = Farengheit * (-17.2222222);
		System.out.println(Farengheit + " Farengheit is " + Celsius + " Celsius ");
		input.close();
		
	}

}
