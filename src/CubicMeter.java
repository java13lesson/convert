package src;
import java.util.Scanner;

public class CubicMeter {

	public static void main(String[] args) {
		double cubicmeter; // 1 cl = 1000 l
		double liter; // 1l = 0.001 cl

		Scanner input = new Scanner(System.in);
		System.out.println("Enter a number in cubicmeter");
		cubicmeter = input.nextDouble();
		liter = cubicmeter / 0.001;
		System.out.println(cubicmeter + " cubicmeter is " + liter + " liter");

		System.out.println("Enter a number in liter");
		liter = input.nextDouble();
		cubicmeter = liter / 1000;
		System.out.println(liter + " liter is " + cubicmeter + " cubicmeter");
		input.close();
	}

}
